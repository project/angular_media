2015-06-25: jlyon: Improved method that angular app is bound
2015-06-25: jlyon: Moved angular app into main module code (rather than requiring a download to sites/all/libraries)
2015-06-25: jlyon: Added CKEditor config steps to README
2015-06-25: jlyon: Fixed issue where placeholder widget would flash at 2x height on page load
2015-06-25: jlyon: Remove duplate version of angular_media.field.inc that is no longer being used.
